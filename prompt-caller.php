<?php
	require "./Services/Twilio.php";
	 
	$response = new Services_Twilio_Twiml();

	$response->pause(array(
		'length' => '1'
	));

	$gather = $response->gather(array(
		'action' => 'handle-caller-input.php',
		'method' => 'POST',
		'numDigits' => '1',
	));

	$ivr_prompt = 'Welcom to the Mount Baker Vapor I V R Demo...
		To listen to our juice ordering menu, press 1...
		To speak to sales, press 2...
		To speak to support, press 3';

	$gather->say(
		$ivr_prompt, 
		array('voice' => 'alice',
	));

	header('Content-Type: text/xml');
	print $response;
