<?php	 
	require "./Services/Twilio.php";

	$response = new Services_Twilio_Twiml();


	$gather = $response->gather(array(
		'action' => 'handle-juice-order.php',
		'method' => 'POST'
	));

	$ivr_prompt = 'This is the juice ordering menu. Please enter the number of juice packets you wish to order, then press pound.';

	$gather->say(
		$ivr_prompt, 
		array('voice' => 'alice',
	));
	
	header('Content-Type: text/xml');
	print $response;
