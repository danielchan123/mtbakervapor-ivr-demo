<?php	 
	require "./Services/Twilio.php";

	$digit = isset($_REQUEST['Digits']) ? $_REQUEST['Digits'] : null;

	$twilio_phone_number = isset($_REQUEST['To']) ? $_REQUEST['To'] : null;
	$response = new Services_Twilio_Twiml();

	if ($digit == 1){
		$response->redirect('prompt-juice-order.php');
	}
	else if ($digit == 2){
		$response->say("This is the sales line.", array('loop' => 5, 'voice' => 'alice'));
	}
	else if ($digit == 3){
		$response->say("This is the support line.", array('loop' => 5, 'voice' => 'alice'));
	}
	
	header('Content-Type: text/xml');
	print $response;
