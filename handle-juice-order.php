<?php	 
	require "./Services/Twilio.php";

	$response = new Services_Twilio_Twiml();

	$digits = isset($_REQUEST['Digits']) ? $_REQUEST['Digits'] : null;

	$response->say('Thank you for ordering ' . $digits .' juice packets. Your order will arrive within 3 business days. Goodbye.', 
		array('voice' => 'alice'));
	
	header('Content-Type: text/xml');
	print $response;
